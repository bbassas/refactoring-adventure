import java.util.Random; 
public class Vampire
{
    
    private static Random randomGenerator = new Random();
    
      public String generateRandomInteraction()
    {
        String vampireSpeaks = "\nYou run into a person who genuinely believes they are vampire. ";
        int randomInteraction = randomGenerator.nextInt(4);
        
        if (randomInteraction == 0)
        {
            vampireSpeaks += ("The vampire starts dancing wildly with you.");
        }
        else if (randomInteraction == 1)
        {
            vampireSpeaks += ("The vampire convinces you to join their cult.");
        }
        else if (randomInteraction == 2)
        {
           vampireSpeaks += ("You pull out a cross and scare the vampire.");
        }
        else if (randomInteraction == 3)
        {
            vampireSpeaks += ("You have an intellectual conversation about blood types with the vampire.");
        }
        
        return vampireSpeaks;
    }
    
      public String toString()
    {
        
        return String.format("%s",generateRandomInteraction() );
    }

}