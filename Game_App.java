//Based on a real bar located off of bourbon street. 

import java.util.Scanner;
public class Game_App

{
    private static boolean gameOver;
    private static String userInput;
    static Dungeon newDungeon = new Dungeon();
    static Room currentRoom = newDungeon.getRoom0();
    private static Room nextRoom;
    static String vampireSurprise = new Vampire().generateRandomInteraction();
    static String mainRoomSurprise = new GothPeople().generateRandomInteraction();
    
    public static void main(String[] args)
    {
        
        Scanner input = new Scanner(System.in);
        Vampire vampireSurprise = new Vampire();
        GothPeople mainRoomSurprise = new GothPeople();
        System.out.println("You are walking down Bourbon Street and start to get irritated by the tourists. You decide to make a right on Toulouse Street. You see a long dark alley and decide to head down it...");
        while (gameOver == false)
        {
        System.out.print(currentRoom);
          if(newDungeon.hiddenRoomCheck(currentRoom) == true )
            {
                vampireSurprise.generateRandomInteraction();
                System.out.print(vampireSurprise);
            }
         else if(newDungeon.mainRoomCheck(currentRoom) == true)
         {
             mainRoomSurprise.generateRandomInteraction();
             System.out.println("\n");
             System.out.print(mainRoomSurprise);
         }
        System.out.println("\n");
        userInput = input.next();
        executeUserChoice();
        currentRoom = nextRoom;
        }
    }
    public static void executeUserChoice()
    {
        
        
        if (userInput.equals("N") || userInput.equals("n") )
        {
            nextRoom = currentRoom.getNorth();
        }
        else if (userInput.equals("S") || userInput.equals("s") )
        {
            nextRoom = currentRoom.getSouth();
        }
        else if (userInput.equals("E") || userInput.equals("e") )
        {
            nextRoom = currentRoom.getEast();
        }
        else if (userInput.equals("W") || userInput.equals("w") )
        {
            nextRoom = currentRoom.getWest();
        }
        
       else if (userInput.equals("Q") || userInput.equals("q") )
        {
                System.out.println("This place is too loud and weird, you decide to leave.");
                
                gameOver = true;
        }
        else
        {
            System.out.println("\nInvalid choice.");
        }
        
        if (nextRoom == null)
        {
            System.out.println("\nYou can't go that way.");
            nextRoom = currentRoom;
        }
    }
}