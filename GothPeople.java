import java.util.Random; 
public class GothPeople
{
    
    private static Random randomGenerator = new Random();
    
      public String generateRandomInteraction()
    {
        String gothPeople = "";
        int randomInteraction = randomGenerator.nextInt(3);
        
        if (randomInteraction == 0)
        {
            gothPeople += ("An androgynous person starts hitting on you. You are intrigued but confused and move on.");
        }
        else if (randomInteraction == 1)
        {
            gothPeople += ("You decide to join the others in a drink of absithne.");
        }
        else if (randomInteraction == 2)
        {
           gothPeople += ("A real bat suddenly flies over the crowd.");
        }
        
        return gothPeople;
    }
    
      public String toString()
    {
        
        return String.format("%s",generateRandomInteraction() );
    }

}