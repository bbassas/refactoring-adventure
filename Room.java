
//define properties getters and setters
public class Room 

{
    private Room north;
    private Room south;
    private Room east;
    private Room west;
    private String descirption;
    
    
    public Room(String descirption)
    {
        
        this.descirption = descirption;
    }
    
    public void setNorth(Room north)
    {
        this.north = north;
    }
    
    public void setSouth(Room south)
    {
        this.south = south;
    }
    
    public void setEast(Room east)
    {
        this.east = east;
    }
    
    public void setWest(Room west)
    {
        this.west = west;
    }
    
    public Room getNorth()
    {
        return this.north;
    }
    
     public Room getSouth()
    {
        return this.south;
    }
    
     public Room getEast()
    {
       return this.east;
    }
    
     public Room getWest()
    {
        return this.west;
    }
    
     public String getDescription()
    {
       return descirption;
    }
    
      public void setExits(Room north,Room south,Room east, Room west)
    {
        this.north = north;
        this.south = south;
        this.east = east;
        this.west = west;
    }
    
     public String getExits()
     
    {
        String exits = "\nSelect n for north\nSelect s for south\nSelect e for east\nSelect w for west\nSelect q to quit\n\nYou can head";
        if (this.north != null)
        {
            exits = exits.concat(" north");
        }
        
         if (this.south != null)
        {
            exits += " south";
        }
        
         if (this.east != null)
        {
            exits += " east";
        }
         if (this.west != null)
        {
            exits += " west";
        }
        return exits;
    }
     public String toString()
    {
        
        return String.format("%s %s",getDescription(),getExits());
    }
}