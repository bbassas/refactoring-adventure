
public class Dungeon 

{
    private Room alley;
    private Room courtyard;
    private Room mainRoom;
    private Room bathRoom;
    private Room hiddenRoom;
    
    
    //Dungeon constructor
    public Dungeon()
    {
       //room descriptions
       alley = new Room("\nYou are in a long, dark alley way. The walls and the floor appear to be made out of cobblestone.");
       courtyard = new Room("\nYou are in a courtyard. There are trees and a waterfoutain. Loud industrial music blairs from a distance.");
       mainRoom = new Room("\nYou are in a bar. You see gothic people everywhere drinking absinthe.");
       bathRoom = new Room("\nYou are in the bathroom. There is complimentary depeche mode CD's.");
       hiddenRoom = new Room("\nYou found the hidden room. There are blood stains on the floor.");
       
       //navigation
       this.alley.setNorth(courtyard);
       this.courtyard.setNorth(mainRoom);
       this.courtyard.setSouth(alley);
       this.mainRoom.setWest(bathRoom);
       this.mainRoom.setSouth(courtyard);
       this.bathRoom.setEast(mainRoom);
       this.bathRoom.setSouth(hiddenRoom);
       this.hiddenRoom.setNorth(bathRoom);
       
    }
    
    public Room getRoom0()
    {
        
        return this.alley;
    }
    
    public boolean hiddenRoomCheck(Room currentRoom)
    {
        boolean hiddenRoomChecked = false;
        if (hiddenRoom.equals(currentRoom) )
        {
            hiddenRoomChecked = true;
        }
        return hiddenRoomChecked;
    }
    
    public boolean mainRoomCheck(Room currentRoom)
    {
        boolean mainRoomChecked = false;
        if (mainRoom.equals(currentRoom) )
        {
            mainRoomChecked = true;
        }
        return mainRoomChecked;
    }
    
}